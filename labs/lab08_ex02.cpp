template<typename T>
class Queue
{
	Queue();
	~Queue();

	void enqueue(T& element);
	bool dequeue(T& element);
	bool peek(T& element);
	bool isEmpty();
	void makeEmpty();
	
}