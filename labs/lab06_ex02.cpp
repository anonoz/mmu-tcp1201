#include <iostream>
#include <string>
using namespace std;
 
class Point {
  int x;
  int y;
public:
  Point(int seed = 0) : x(seed), y(seed) {
    cout << "Overloaded constructor called.\n";
  }
  ~Point() { cout << "Destructor called.\n"; }
  int getX() { return x; }
  int getY() { return y; }
};
 
ostream& operator<< (ostream& os, Point& p) {
  os << "(" << p.getX() << "," << p.getY() << ")";
  return os;
}
 
template <typename T>
struct Node {
  T info;
  Node<T> *next;
};
 
template <typename T>
void print (Node<T> *start) {
  if (start == NULL) {        // cout a meesage if the list is empty.
    cout << "List is empty.\n";
    return;
  }
  Node<T> *ptr = start;
  while (ptr != NULL) {       // while not at the end of list.
    cout << ptr->info << " "; // cout the current node’s info.
    ptr = ptr->next;          // move to next node.
  }
  cout << endl;
}
 
int main() {
  int seed = 0;         // seed for automatic value of x and y.
  // Add codes.
  Node<Point> *start = NULL;

  int choice; // user choice.
  Point newPoint;
  do {
    // Add codes.
    print(start);

    cout << "Choice:\n"
         << "1: Insert new point at the front\n"
         << "2: Insert new point at the back\n"
         << "3: Remove point at the front\n"
         << "4: Remove point at the back\n"
         << "Others: Exit\n";
    cin >> choice;
    switch (choice) {
      case 1 :
      {       
               newPoint = Point(++seed);
               // Add codes.
               Node<Point> *ptr = new Node<Point>;
               ptr->info = newPoint;
               ptr->next = start;

               start = ptr;
               break;
      }
      case 2 : 
      {
               newPoint = Point(++seed);
               // Add codes.
               Node<Point> *ptr = new Node<Point>;
               ptr->info = newPoint;
               ptr->next = NULL;

               if (start == NULL)
               {
                 start = ptr;
                 break;
               }

               Node<Point> *current = start;
               while (current->next != NULL)
               {
                 current = current->next;
               }

               current->next = ptr;

               break;
      }
      case 3 : // Add codes.
      {
               Node<Point> *new_start = start->next;
               delete start;
               start = new_start;
               break;
      }
      case 4 : // Add codes.
               Node<Point> *current = start;
               while (current->next->next != NULL)
               {
                 current = current->next;
               }

               delete current->next;
               current->next = NULL;
               break;
    }
  } while (choice >= 1 && choice <= 4);
  // Add codes.
}