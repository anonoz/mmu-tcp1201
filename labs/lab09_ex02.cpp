#include <iostream>

void fibonacci(int x, int y, int limit)
{
	if (limit < 1) return;

	int z = x + y;

	if (z == 0) z = 1;

	std::cout << z << ' ';

	fibonacci(y, z, limit - 1);
}

int main()
{
	fibonacci(0, 0, 20);
}