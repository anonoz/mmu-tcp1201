#include <iostream>

template<typename T>
class BinarySearchTree
{
	T info;
	BinarySearchTree<T>* left;
	BinarySearchTree<T>* right;

public:
	BinarySearchTree(T item): info(item)
	{
		left  = NULL;
		right = NULL;
		std::cout << "Initialized BST with node " << info << std::endl;
	};

	~BinarySearchTree()
	{
		makeEmpty();
	}

	bool isEmpty()
	{
		return left == NULL && right == NULL;
	}

	void makeEmpty()
	{
		std::cout << "Deleting node " << this->info << std::endl;
		if (left  != NULL){ left->makeEmpty();  left = NULL; delete left;  }
		if (right != NULL){ right->makeEmpty(); right = NULL; delete right; }

		return;
	}

	void insert(const T& item)
	{
		// Cases of empty children
		if (item < info && left == NULL)
		{	
			std::cout << "Inserted " << item << " at left node." << std::endl;
			left = new BinarySearchTree<T>(item);
		}
		else if (item >= info && right == NULL)
		{
			std::cout << "Inserted " << item << " at right node." << std::endl;
			right = new BinarySearchTree<T>(item);
		}
		// Cases of got children
		else if (item < info && left != NULL)
		{
			std::cout << "Delegated insertion of " << item << " to left node " << left->info << "." << std::endl;
			left->insert(item);
		}
		else if (item >= info && right != NULL)
		{
			std::cout << "Delegated insertion of " << item << " to right node " << right->info << "." << std::endl;
			right->insert(item);
		}

		return;
	}

	bool search(const T& item)
	{
		return (item == info || (left != NULL && left->search(item)) || (right != NULL && right->search(item)));
	}

	void preOrderTraversal()
	{
		std::cout << this->info;

		if (left  != NULL)
		{
			std::cout << ", ";
			left->preOrderTraversal();
		}

		if (right != NULL)
		{
			std::cout << ", ";
			right->preOrderTraversal();
		}
	}

	void inOrderTraversal()
	{
		if (left  != NULL)
		{
			left->inOrderTraversal();
			std::cout << ", ";
		}

		std::cout << this->info;

		if (right != NULL)
		{
			std::cout << ", ";
			right->inOrderTraversal();
		}
	}

	void postOrderTraversal()
	{
		if (left  != NULL)
		{
			left->postOrderTraversal();
			std::cout << ", ";
		}

		if (right != NULL)
		{
			right->postOrderTraversal();
			std::cout << ", ";
		}

		std::cout << this->info;
	}
};

int main()
{
	// Test create BST with 10
	BinarySearchTree<int>* tree = NULL;

	int input;

	do {
		std::cout << "Type a number, or 0 to finish: ";
		std::cin >> input;

		if (input == 0)
		{
			break;
		}
		else
		{
			if (tree == NULL)
			{
				tree = new BinarySearchTree<int>(input);
			}
			else
			{
				tree->insert(input);
			}
		}
	} while (true);

	std::cout << "\nPre Order Traversal:" << std::endl;
	tree->preOrderTraversal();

	std::cout << "\nIn Order Traversal:" << std::endl;
	tree->inOrderTraversal();

	std::cout << "\nPost Order Traversal:" << std::endl;
	tree->postOrderTraversal();

	std::cout << "\nIs 29 present? " << ((tree->search(29)) ? "Yes":"no") << std::endl;
	std::cout << "How about 14? " << ((tree->search(14)) ? "Yes":"no") << std::endl;

	delete tree;

	return 0;
}