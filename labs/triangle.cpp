#include <iostream>
#include "point.hpp"
#include "triangle.hpp"

using namespace std;

void Triangle::calcPerimeter()
{
  // TODO 
  double tempPerimeter = 0.00;
  
  // 1st side
  tempPerimeter += p[0].distance(p[1]);
  tempPerimeter += p[1].distance(p[2]);
  tempPerimeter += p[2].distance(p[0]);
 
  this->perimeter = tempPerimeter;
}
 
void Triangle::set3Points()
{
  cout << "Enter coordinates: " << endl;
  for(int i = 0; i <= 2; i++)
  {
    int tempX, tempY;
    cin >> tempX  >> tempY;
    p[i].setX(tempX);
    p[i].setY(tempY);
  }
}
 
void Triangle::display()
{
  this->calcPerimeter();
  cout << "perimeter: " << this->perimeter << endl;
}