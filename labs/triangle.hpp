#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "point.hpp"

class Triangle
{
  Point p[3];
  double perimeter;
  void calcPerimeter();
 public: 
  void set3Points();
  void display();
};

#endif