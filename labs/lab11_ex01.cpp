#include <iostream>
#include <time.h>
#include <algorithm>
#include <vector>
#include <set>

template<typename T>
bool avsb(T x, T y)
{
  return x > y;
}

int main()
{
  srand(time(NULL));

  // Vector
  std::vector<int> random_vector;
  for (int i = 0; i < 10; i++) random_vector.push_back(rand() % 10);

  std::cout << "Vector Ascending..." << std::endl;
  std::sort(random_vector.begin(), random_vector.end());
  for (int i = 0; i < 10; i++)
    std::cout << random_vector[i] << " - ";

  std::cout << std::endl;

  std::cout << "Vector Descending..." << std::endl;
  std::sort(random_vector.begin(), random_vector.end(), std::greater<int>());
  for (int i = 0; i < 10; i++)
    std::cout << random_vector[i] << " - ";

  std::cout << std::endl;

  // Unique with set
  std::set<int> random_set(random_vector.begin(), random_vector.end());
  // for (int i = 0; i < 10; i++) random_set.insert(rand() % 10);

  // Show ascending
  std::cout << "Set Ascending..." << std::endl;
  std::set<int>::iterator random_set_interator = random_set.begin();
  while (random_set_interator != random_set.end())
    std::cout << *random_set_interator++ << " - ";

  std::cout << std::endl;

  // Show descending
  std::cout << "Set Descending..." << std::endl;
  while (random_set_interator != random_set.begin())
  	std::cout << *--random_set_interator << " - ";

  std::cout << std::endl;

  // Find
  int target;
  std::cout << "Find a number (0 - 9): ";
  std::cin >> target;
  std::cin.ignore();

  if (random_set.end() != std::find(random_set.begin(), random_set.end(), target))
  	std::cout << "Found it!" << std::endl;
  else
  	std::cout << "Not Found" << std::endl;

  return 0;
}