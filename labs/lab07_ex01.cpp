#include <iostream>
#include <string>
#include <vector>
#include <cstdlib> // system("PAUSE");
using namespace std;
 
int main() {
 vector<int> v1;
 cout << "Adding 20 items to vector.\n";
 cout << "vector automatically doubles the capacity of its internal dynamic array when it is full.\n";
 cout << "Size\tCapacity\n";
 cout << v1.size() << "\t" << v1.capacity() << endl;
 for (int i = 0; i < 20; i++) {
   v1.push_back (i);
   cout << v1.size() << "\t" << v1.capacity() << endl;
 }
 
 system("PAUSE");
 
 cout << "Removing the items in vector.\n";
 cout << "vector never automatically reduces its array capacity.\n";
 cout << "Size\tCapacity\n";
 while (!v1.empty()) {
   v1.erase (v1.begin());
   cout << v1.size() << "\t" << v1.capacity() << endl;
 }
}
 