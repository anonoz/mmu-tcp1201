#include <iostream>
#include "triangle.hpp"
#include "point.hpp"

using namespace std;

int main()
{
  Point p[] = { Point(), Point(1), Point(2, 3) };

  for (int i = 0; i < 3; i++)
  	cout << "(" << p[i].getX() << ", " << p[i].getY() << ")\n";

  cout << "\nTriangle ";

  Triangle t;
  t.display();
}