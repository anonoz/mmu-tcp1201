#ifndef POINT_HPP
#define POINT_HPP

class Point
{
  friend bool operator== (Point, Point);
  friend std::ostream& operator<< (std::ostream&, Point&);
  friend std::istream& operator>> (std::istream&, Point&);

  int x;
  int y;
public:
  Point(){};
  Point(int x, int y): x(x), y(y){};
  void setX(int x);
  void setY(int y);
  int getX();
  int getY();
  double distance (Point& p);
};

#endif