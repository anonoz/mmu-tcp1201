#include <iostream>
#include "rational.hpp"

int Rational::getNum() const
{
  return this->num;
}

int Rational::getDen() const
{
  return this->den;
}

void Rational::print()
{
  std::cout << this->num << "/" << this->den;
}

Rational operator+ (Rational& r1, Rational& r2)
{
  int combined_den, combined_num;

  combined_den = r1.getDen() * r2.getDen();
  combined_num = r2.getDen() * r1.getNum() + r1.getDen() * r2.getNum();

  return Rational(combined_num, combined_den);
};

Rational operator- (Rational& r1, Rational& r2)
{
  int combined_den, combined_num;

  combined_den = r1.getDen() * r2.getDen();
  combined_num = r2.getDen() * r1.getNum() - r1.getDen() * r2.getNum();

  return Rational(combined_num, combined_den);
};

bool operator< (Rational& r1, Rational& r2)
{
  return r1.getNum() * r2.getDen() < r2.getNum() * r1.getDen();
};

bool operator> (Rational& r1, Rational& r2)
{
  return r1.getNum() * r2.getDen() > r2.getNum() * r1.getDen();
};

bool operator== (Rational& r1, Rational& r2)
{
  return r1.getNum() * r2.getDen() == r2.getNum() * r1.getDen();
};

std::istream& operator>> (std::istream& is, Rational& r)
{
  is >> r.num >> r.den;
  return is;
};

std::ostream& operator<< (std::ostream& os, const Rational& r)
{
  os << r.getNum() << "/" << r.getDen();
  return os;
};