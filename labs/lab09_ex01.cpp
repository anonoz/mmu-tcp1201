#include <iostream>

long long recursiveFactorial(int n)
{
	return (n <= 0) ? 1 : n * recursiveFactorial(n - 1);
}

long long nonRecursiveFactorial(int n)
{
	long long result = 1;
	int x = 0;

	while (x < n)
	{
		x++;
		result = result * x;
	}

	return result;
}

int main()
{
	std::cout << "Non-recursive: " << nonRecursiveFactorial(9) << std::endl
		      << "Recursive    : " << recursiveFactorial(9) << std::endl;
}