#ifndef RATIONAL_HPP_INCLUDED
#define RATIONAL_HPP_INCLUDED

class Rational
{
  friend Rational operator+ (Rational&, Rational&);
  friend Rational operator- (Rational&, Rational&);
  friend bool operator< (Rational&, Rational&);
  friend bool operator> (Rational&, Rational&);
  friend bool operator== (Rational&, Rational&);
  friend std::istream& operator>> (std::istream&, Rational&);
  friend std::ostream& operator<< (std::ostream&, const Rational&);

  int num, den;

public:
  Rational(int num = 0, int den = 0)
    : num(num), den(den)
  {
  };

  int getNum() const;
  int getDen() const;

  void print();

};

#endif