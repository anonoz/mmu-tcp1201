#include <iostream>
#include <string>
using namespace std;
 
class Point {
  int x;
  int y;
public:
  Point(int seed = 0) : x(seed), y(seed) {
    cout << "Overloaded constructor called.\n";
  }
  ~Point() { cout << "Destructor called.\n"; }
  int getX() { return x; }
  int getY() { return y; }
};
 
ostream& operator<< (ostream& os, Point& p) {
  os << "(" << p.getX() << "," << p.getY() << ")";
  return os;
}
 
template <typename T>
void print (T *array, int size) {
  if (size == 0) {
    cout << "Array is empty.\n";
    return;
  }
  for (int i = 0; i < size; i++)
    cout << array[i] << " ";
  cout << endl;
}
 
// Add codes.
template <typename T>
void insertFront(T* &points, int &size, T point)
{
  T* new_points = new T[size + 1];

  // deep copy?
  for (int i = 0; i < size; i++)
    new_points[i + 1] = points[i];

  new_points[0] = point;
  size++;

  points = new_points;
}

template <typename T>
void insertBack(T* &points, int &size, T point)
{
  T* new_points = new T[size + 1];

  // deep copy?
  for (int i = 0; i < size; i++)
    new_points[i] = points[i];

  new_points[size] = point;
  size++;

  delete [] points;
  points = new_points;
}

template <typename T>
void removeFront(T* &points, int &size)
{
  T* new_points = new T[size - 1];

  for (int i = 0; i < size - 1; i++)
    new_points[i] = points[i + 1];

  size--;
  points = new_points;
}
 
template <typename T>
void removeBack(T* &points, int &size)
{
  T* new_points = new T[size - 1];

  for (int i = 0; i < size - 1; i++)
    new_points[i] = points[i];

  size--;
  points = new_points;
}

int main() {
  int seed = 0;         // seed for automatic value of x and y.
  int size = 0;         // array size.
  Point *points = NULL; // dynamic array.
  int choice; // user choice.
  Point newPoint;
  do {
    print (points, size);
    cout << "Choice:\n"
         << "1: Insert new point at the front\n"
         << "2: Insert new point at the back\n"
         << "3: Remove point at the front\n"
         << "4: Remove point at the back\n"
         << "Others: Exit\n";
    cin >> choice;
    switch (choice) {
      case 1 : newPoint = Point(++seed);
               insertFront (points, size, newPoint); break;
      case 2 : newPoint = Point(++seed);
               insertBack (points, size, newPoint); break;
      case 3 : removeFront (points, size); break;
      case 4 : removeBack (points, size); break;
    }
  } while (choice >= 1 && choice <= 4);
  print (points, size);
  if (size > 0)
    delete [] points;
}