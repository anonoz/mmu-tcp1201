#include <iostream>
#include <string>
#include <algorithm>
#include <map>

int main()
{
  std::multimap<std::string, int> lecturer_students;

  std::string lecturer_name;
  int student_id;
  for (int i = 10; i > 0; i--)
  {
  	std::cout << i << " more to go:-" << std::endl;

    // lecturer
    std::cout << "Enter lecturer's name: ";
    std::getline(std::cin, lecturer_name);

    // student
    std::cout << "Enter student's ID: ";
    std::cin >> student_id;
    std::cin.ignore();

    // push
    lecturer_students.insert(
      std::pair<std::string, int>(lecturer_name, student_id)
    );
  }

  std::cout << "Multimap Ascending By Default..." << std::endl;
  std::map<std::string, int>::iterator ls_iterator;
  ls_iterator = lecturer_students.begin();

  while (ls_iterator != lecturer_students.end())
  {
    std::cout << ls_iterator->first << " - " << ls_iterator->second << std::endl;
    ls_iterator++;
  }

  // Descending
  std::cout << "Multimap Descending..." << std::endl;

  std::multimap<std::string, int, std::greater<std::string> > lecturer_students_desc(
  	lecturer_students.begin(), lecturer_students.end());

  ls_iterator = lecturer_students_desc.begin();

  while (ls_iterator != lecturer_students_desc.end())
  {
    std::cout << ls_iterator->first << " - " << ls_iterator->second << std::endl;
    ls_iterator++;
  }

  // You are trapped
  while (true)
  {
    // Find, kena use equal_range
    std::cout << "Find students belong to lecturer name: ";
    std::getline(std::cin, lecturer_name);

    std::pair <std::multimap<std::string, int>::iterator, std::multimap<std::string, int>::iterator> results;
    results = lecturer_students.equal_range(lecturer_name);

    std::cout << lecturer_name << " advises: ";

    for (std::multimap<std::string, int>::iterator it = results.first; it != results.second; it++)
    	std::cout << it->second << ", ";

    std::cout << std::endl;
  }

  return 0;
}
