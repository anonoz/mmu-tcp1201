#include <iostream>
#include <vector>
#include "point.hpp"

using namespace std;

bool SortByXY(Point& p1, Point& p2)
{
  if (p1.getX() != p2.getX())
    return p1.getX() < p2.getX();
  else
    return p1.getY() < p2.getY();
}

bool SortByYX(Point& p1, Point& p2)
{
  if (p1.getY() != p2.getY())
    return p1.getY() < p2.getY();
  else
    return p1.getX() < p2.getX();
}

int main() {
  vector<Point> v;
  Point p;
  do {
    cout << "Input a Point (0 0 to end): ";
    cin >> p;
    if (p == Point(0,0)) break;
    v.push_back (p);
  } while (true);
  
  cout << "Sort points by x then y:\n";
  sort (v.begin(), v.end(), SortByXY);
  for (int i = 0; i < v.size(); i++)
    cout << v[i] << " ";
  cout << endl;
  
  cout << "Sort points by y then x:\n";
  sort (v.begin(), v.end(), SortByYX);
  for (int i = 0; i < v.size(); i++)
    cout << v[i] << " ";
  cout << endl;
}