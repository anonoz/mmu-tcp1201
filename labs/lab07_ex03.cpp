#include <iostream>
#include <string>
using namespace std;
 
class Point {
 int x;
 int y;
public:
 Point (int x = 0, int y = 0) : x(x), y(y) {}
 friend ostream& operator<< (ostream& os, Point& p) {
   // Add code.
   os << p.x << " " << p.y;
   return os;
 }
 friend istream& operator>> (istream& is, Point& p) {
   cout << "Input x y: ";
   // Add code.
   is >> p.x >> p.y;
   return is;
 }
 friend bool operator== (Point& left, Point& right) {
// Add code.
   return left.x == right.x && left.y == right.y;
 }
};
 
template <typename T>
struct Node {
 T info;
 Node<T> *next;
};
 
template <typename T>
class LinkedList {
 Node<T> *start;
public:
 LinkedList() {
   start = NULL;
 } 
 
 ~LinkedList() {
   // Add code.
   makeEmpty();
 }
 
 void insertFront (T& newElement) {
   Node<T> * new_node = new Node<T>;
   new_node->info = newElement;
   new_node->next = start;

   start = new_node;
 }
 
 void insertBack (T& newElement) {
   Node<T> * current_node = start;

   while(current_node->next != NULL)
   {
     current_node = current_node->next;
   }

   Node<T> * new_node = new Node<T>;
   new_node->info = newElement;
   new_node->next = NULL;

   current_node->next = new_node;
 }
 
 bool insertAfter (T& target, T& newElement) {
   Node<T> * target_node = start;

   if (start == NULL) return false;

   while (target_node != NULL && !(target_node->info == target)) // short circuiting
   {
     target_node = target_node->next;
   }

   if (target_node == NULL)
   {
     return false;
   }

   Node<T>* new_node = new Node<T>;
   new_node->info = newElement;
   new_node->next = target_node->next;

   target_node->next = new_node;

   return true;
 }
 
 bool search (T& target) {
   Node<T> * target_node = start;

   if (start == NULL) return false;

   while (target_node != NULL && !(target_node->info == target))
   {
     target_node = target_node->next;
   }

   if (target_node == NULL)
   {
     return false;
   }

   return true;
 }
 
 void makeEmpty () {
   Node<T> * current_node = start;

   while (current_node->next != NULL)
   {
     Node<T> * next_node = current_node->next;
     delete current_node;
     current_node = NULL;
     current_node = next_node;
   }

   start = NULL;

   return;
 }

 bool remove(T& target)
 {
   Node<T> * target_node = start;
   Node<T> * previous_node;

   while(target_node != NULL && !(target_node->info == target))
   {
     previous_node = target_node;
     target_node = target_node->next;
   }

   // No point found
   if (target_node == NULL) return false;

   // Point is start
   if (target_node == start)
   {
     start = target_node->next;
     delete target_node;
     return true;
   }

   // Point is end
   if (target_node->next == NULL)
   {
     previous_node->next = NULL;
     delete target_node;
     return true;
   }

   // Point is middle
   previous_node->next = target_node->next;
   delete target_node;

   return true;
 }
 
 friend ostream& operator<< (ostream& os, LinkedList<T>& list) {
   Node<T> * current_node = list.start;

   if (list.start == NULL) os << "Nothing in the list." << endl;

   while(current_node != NULL)
   {
     os << current_node->info << endl;
     current_node = current_node->next;
   }

   return os;
 }
};
 
int main() {
 int seed = 0;         // seed for automatic value of x and y.
 LinkedList<Point> list;
 int choice; // user choice.
 bool found;
 Point newPoint, target;
 do {
   cout << "Choice:\n"
        << "1: Display points\n"
        << "2: Insert new point at the front\n"
        << "3: Insert new point at the end\n"
        << "4: Insert new point after a target\n"
        << "5: Search a point\n"
        << "6: Empty the list\n"
        << "7: Delete a point\n"
        << "Others: Exit\n";
   cin >> choice;
   switch (choice) {
     case 1 : cout << list << endl;
              break;
     case 2 : newPoint = Point(++seed, seed);
              list.insertFront (newPoint);
              break;
     case 3 : newPoint = Point(++seed, seed);
              list.insertBack (newPoint);
              break;
     case 4 : cin >> target;
              newPoint = Point(++seed, seed);
              found = list.insertAfter (target, newPoint);
              if (!found) cout << "Target not found\n";
              break;
     case 5 : cin >> target;
              found = list.search (target);
              if (!found) cout << "Target not found\n";
              else       cout << "Target found\n";
              break;
     case 6 : list.makeEmpty();
              cout << "List is emptied\n\n";
              break;
     case 7 : cin >> target;
              cout << ((list.remove(target)) ? "Point deleted. \n":"Point not found. 404. \n") << endl;
              break;
   }
 } while (choice >= 1 && choice <= 7);
}