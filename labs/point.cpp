#include <iostream>
#include <math.h>
#include "point.hpp"

using namespace std;

void Point::setX(int x)
{
  this->x = x;
}
 
void Point::setY(int y)
{
  this->y = y;
}

int Point::getX(){ return this->x; }
int Point::getY(){ return this->y; }
 
double Point::distance(Point& p)
{
  return sqrt(pow(p.getX() - this->getX(), 2) + pow(p.getY() - this->getX(), 2));
}

bool operator== (Point p1, Point p2)
{
  return p1.x == p2.x && p1.y == p2.y;
}

ostream& operator<< (ostream& os, Point& p)
{
  os << "(" << p.x << ", " << p.y << ")";
  return os;
}

istream& operator>> (istream& is, Point& p)
{
  is >> p.x >> p.y;
  return is;
}