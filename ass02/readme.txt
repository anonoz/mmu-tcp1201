Assignment 02
=============

Student Info
------------
Chong Hon Siong
1131123100
TC02 TT04
honsiongchs@gmail.com
017-3009142

http://www.github.com/anonoz

Instruction
-----------
Very simple one.

Run the `make` in the source code directory and launch the app.

Follow on-screen instructions, you will probably need to type in serial number or search keywords, other than that keep on pressing enter.

Enjoy!
