#ifndef XSTACK_HPP_INCLUDED
#define XSTACK_HPP_INCLUDED

#include "node.hpp"

template<typename T>
class XStack {
	Node<T> * start;

public:
	XStack();
	XStack(XStack<T>*&);

	friend std::ostream& operator << (std::ostream&, const XStack<T>&);
	std::string dump();

	void push(T& thing);
	bool pop();
	bool pop(T& thing);
	T peek();
	bool peek(T& thing);

	bool remove(T& thing);

	bool isEmpty();
	void makeEmpty();

	bool duplicate();
	bool swapTop();
	bool roll(int);

	int count();

	Node<T> * getStart();

	void flip();

};

#endif