#ifndef BOOKITEM_HPP
#define BOOKITEM_HPP
#include <iostream>
#include <string>
#include "XStack.hpp"
using namespace std;

class BookItem
{
    int serialNum;
    string title;
    string author;
    int yearPublished;
    int copiesSold;
public:
	BookItem(){};
    BookItem(int serialNum, string title, string ath, int year, int copiesSold)
    : serialNum(serialNum), title(title), author(ath), yearPublished(year), copiesSold(copiesSold) { };

    BookItem(int serialNum):serialNum(serialNum){}; // for search
    BookItem(string keyword): title(keyword){}; // for search two

	friend ostream& operator<<(ostream& os, const BookItem& s);
	friend bool operator==(const BookItem&, const BookItem& s);

	string getTitle();
	void setTitle(string);

	int getCopiesSold();
	void setCopiesSold(int);

	int getYearPublished();
	void setYearPublished(int);

	static bool search(BookItem&, XStack<BookItem>*& st,XStack<BookItem>*&);

	static XStack<BookItem> top5(XStack<BookItem>*& st);
	static XStack<BookItem> bottom5(XStack<BookItem>*& st);
	static XStack<BookItem> newest10(XStack<BookItem>*& st);

};

#endif // BOOKITEM_HPP
