#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

#include "node.hpp"
#include "XStack.hpp"

using namespace std;

template<typename T>
XStack<T>::XStack()
{
	start = NULL;
}

template<typename T>
XStack<T>::XStack (XStack<T>*& st)
{
	Node<T> * trace_ptr = st->getStart();
	Node<T> * previous_ptr = NULL;

	while(trace_ptr != NULL)
	{
		// Copy values
		Node<T> * current_ptr = new Node<T>;
		current_ptr->info = trace_ptr->info;
		current_ptr->next = NULL; // default

		// Link from previous node
		if (previous_ptr != NULL)
		{
			previous_ptr->next = current_ptr;
		}

		// If first ptr, set start
		if (previous_ptr == NULL)
		{
			start = current_ptr;
		}

		// Prepare next loop...
		previous_ptr = current_ptr;
		trace_ptr = trace_ptr->next;
	}
}

template<typename T>
ostream& operator << (ostream& os, const XStack<T>& st)
{
	Node<T> * ptr = st.start;

	while (ptr != NULL)
	{
		os << ptr->info;
		ptr = ptr->next;
	}

	os << endl;

	return os;
}

template<typename T>
string XStack<T>::dump()
{
	stringstream ss;
	Node<T> * ptr = start;

	while (ptr != NULL)
	{
		ss << ptr->info;
		ptr = ptr->next;
	}

	return ss.str();
}

template<typename T>
void XStack<T>::push(T& thing)
{
	Node<T> * ptr = new Node<T>;
	ptr->info = thing;
	ptr->next = (start == NULL) ? NULL : start;
	start = ptr;

	return;
}

template<typename T>
bool XStack<T>::pop()
{
	if (start == NULL) return false;

	Node<T> * ptr = start;
	ptr = start->next;

	delete start;
	start = ptr;

	return true;
}

template<typename T>
bool XStack<T>::pop(T& thing)
{
	if (start == NULL) return false;

	Node<T> * ptr = start;
	ptr = start->next;
	thing = start->info;

	delete start;
	start = ptr;

	return true;
}

template<typename T>
T XStack<T>::peek()
{
	if (start == NULL) return false;

	return start->info;
}

template<typename T>
bool XStack<T>::peek(T& thing)
{
	if (start == NULL) return false;

	thing = start->info;
	return true;
}

template<typename T>
bool XStack<T>::remove(T& thing)
{
	Node<T> * ptr = start;
	Node<T> * previous_ptr = NULL;

	while (ptr != NULL)
	{
		if (ptr->info == thing)
		{
			// If node happens to be front
			if (ptr == start)
			{
				if (ptr->next != NULL)
					start = ptr->next;
				else
					start = NULL;
			}
			// or back
			else if (ptr->next == NULL)
			{
				if (previous_ptr != NULL)
					previous_ptr->next = NULL;
			}
			// or average citizen
			else
			{
				previous_ptr->next = ptr->next;
			}
			
			delete ptr;

			return true;
		}

		previous_ptr = ptr;
		ptr = ptr->next;
	}

	return false;
}

template<typename T>
bool XStack<T>::isEmpty()
{
	return start == NULL;
}

template<typename T>
void XStack<T>::makeEmpty()
{
	if (!isEmpty()) 
		pop();
	else
		return;
	return makeEmpty();
}

template<typename T>
bool XStack<T>::duplicate()
{
	if (start == NULL) return false;

	Node<T> * new_ptr = new Node<T>;
	new_ptr->info = start->info;
	new_ptr->next = start;
	start = new_ptr;

	return true;
}

template<typename T>
bool XStack<T>::swapTop()
{
	if (start == NULL) return false;

	Node<T> * first_ptr = start, * second_ptr = start->next;

	start = second_ptr;
	first_ptr->next = second_ptr->next;
	start->next = first_ptr;

	return true;
}

template<typename T>
bool XStack<T>::roll(int num_affected)
{
	// Make sure there are enough nodes to roll
	if (num_affected > count()) return false;

	// K start rolling
	int counter = 1;
	Node<T> * original_start = start;
	Node<T> * ptr = start;

	while (counter <= num_affected)
	{
		// THE BOSS MODE ABCD(E)/F
		if (counter == num_affected)
		{
			ptr->next = original_start;
			start = ptr;

			return true;
		}

		// Right before the finale ABC(D)E/F
		if (++counter == num_affected)
		{
			Node<T> * temp_ptr = ptr->next;
			ptr->next = ptr->next->next; // D-> = E-> == F
			ptr = temp_ptr; // ptr = D->

			continue;
		}
		
		ptr = ptr->next;
	}

	return false; // I dunno, but this should not be returned right
}

template<typename T>
int XStack<T>::count()
{
	Node<T>* ptr = start;
	int counter = 0;

	while (ptr != NULL)
	{
		counter++;
		ptr = ptr->next;
	}

	return counter;
}

template<typename T>
Node<T> * XStack<T>::getStart()
{
	return start;
}

template<typename T>
void XStack<T>::flip()
{
	Node<T> *original_start = start,
			*previous_ptr = NULL,
			*ptr = start;

	while (ptr != NULL)
	{
		// Backup 
		Node<T>* backup_ptr_next = ptr->next;

		// Case for original end
		if (ptr->next == NULL)
		{
			ptr->next = previous_ptr;
			start = ptr;
		}
		// Case for original start
		else if (ptr == original_start)
		{
			ptr->next = NULL;
		}
		// Anyone in between
		else
		{
			ptr->next = previous_ptr;
		}

		// Moving on...
		previous_ptr = ptr;
		ptr = backup_ptr_next;
	}
}
