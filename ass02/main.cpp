#ifndef MAIN_CPP
#define MAIN_CPP
#include <fstream>
#include <iostream>
#include <string>
#include <stack>
#include <cstdlib>
#include "BookItem.hpp"
#include "XStack.hpp"
#include "XStack.cpp"

using namespace std;

/* I MODIFIED THIS S*** EVEN THOUGH I AM NOT FAMILIAR WITH IT! */
void readData(const char* filename, XStack<BookItem>*&stBooks) {
    string line;
    ifstream ifs(filename);
    if (ifs.is_open()) {
        cout << "Reading data...\n";
        int c = 0;
        while ( getline (ifs,line) && (*line.c_str() != '\0') )  {
            string delimiter = ",";
            size_t pos = 0;
            string* token = new string[5];
            int f = -1;
            while ((pos = line.find(delimiter)) != string::npos) {
                token[++f] = line.substr(0, pos);
                cout << " " << token[f] << " | " ;
                line.erase(0, pos + delimiter.length());
            }
            token[++f] = line;
            cout << token[f] << endl;       // last item in string
            c++;

            // push to XStack (numerical data converted to int)
            BookItem b(atoi(token[0].c_str()), token[1], token[2], atoi(token[3].c_str()), atoi(token[4].c_str()));
            stBooks->push(b);
        }
        cout << c << " row(s) read." << endl;
        ifs.close();
    }
    else
        cout << "Unable to open file";
}

int main()
{
    // You are to replace the STL Stack with XStack that you have created
    XStack<BookItem>* stBooks = new XStack<BookItem>();

    // read database from text file, and store in the stack
    readData("db_big.txt", stBooks); cout << endl;

    // Test book searching by serial number
    cout << "BOOK SEARCH\n===========\n";
    int target_serial_num;
    cout << "Key in serial num to find a book (example: 15533): ";
    cin >> target_serial_num;

    XStack<BookItem>* search_results = new XStack<BookItem>();
    BookItem search_target(target_serial_num); // MAJULAH SINGAPURA!!!
    if (BookItem::search(search_target, stBooks, search_results))
        cout << "Found it:\n" << search_results->dump();
    else
        cout << "Cant find the book, something broke?\n";

    cout << endl << "Press enter to continue...";
    cin.ignore();
    cout << '\r';

    // try search keywrods
    string target_keywords;
    cout << "Key in keywords to find book (example: Singapore): ";
    getline(cin, target_keywords);

    search_results->makeEmpty();
    BookItem search_target2(target_keywords); // MAJULAH SINGAPURA!!!
    if (BookItem::search(search_target2, stBooks, search_results))
        cout << "Found it:\n" << search_results->dump();
    else
        cout << "Cant find the book, something broke?\n";

    cout << endl << "Press enter to continue...";
    cin.ignore();
    cout << '\r';

    // Test Pop & Peek
    cout << "\nPOP & PEEK\n==========\n";
    cout << "\nFrom top of stack:\n" << stBooks->peek() << endl;
    stBooks->pop();
    cout << "And pop: " << endl << stBooks->peek() << endl;

    cout << endl << "Press enter to continue...";
    cin.ignore();
    cout << '\r';

    // Test swapTop
    cout << "\nSWAPTOP\n=======\n";

    cout << "Before swapping\n---------------\n";
    cout << stBooks->dump() << endl;

    cout << endl << "Press enter to continue...";
    cin.ignore();
    cout << '\r';

    stBooks->swapTop();
    
    cout << "After swapping\n--------------\n";
    cout << stBooks->dump() << endl;

    cout << endl << "Press enter to continue...";
    cin.ignore();
    cout << '\r';

    // Test Roll
    cout << "ROLL\n====\n";

    cout << "Before the roll\n---------------\n";
    cout << stBooks->dump() << endl;

    cout << endl << "Press enter to continue...";
    cin.ignore();

    stBooks->roll(5);

    cout << "After rolling 5\n---------------\n";
    cout << stBooks->dump() << endl;

    cout << endl << "Press enter to continue...";
    cin.ignore();
    cout << '\r';

    // Test top 5
    cout << "TOP 5\n=====\n";
    cout << BookItem::top5(stBooks).dump();

    cout << endl << "Press enter to continue...";
    cin.ignore();

    // Worst 5
    cout << "WORST 5\n=======\n";
    cout << BookItem::bottom5(stBooks).dump();

    cout << endl << "Press enter to continue...";
    cin.ignore();

    // new coming 10
    cout << "NEW BOOKS!\n==========\n";
    cout << BookItem::newest10(stBooks).dump();

    cout << endl << "Press enter to continue...";
    cin.ignore();

    // Test duplicate
    cout << "DUPLICATE\n==========\n\n";
    cout << "Before Duplication\n------------------\n";
    cout << stBooks->dump() << endl;

    stBooks->duplicate();

    cout << "After Duplication\n-----------------\n";
    cout << stBooks->dump() << endl;

    // Empty methods
    cout << "Wrappin Up\n==========\n";
    cout << "The book stack is " << (stBooks->isEmpty() ? "empty!":"not empty.") << endl;
    stBooks->makeEmpty();
    cout << "But now it is " << (stBooks->isEmpty() ? "empty!":"not empty.") << endl;

    cout << '\n';

	return 0;
}
#endif
