#include <iostream>
#include <string>
#include <algorithm>
#include "node.hpp"
#include "BookItem.hpp"
#include "XStack.hpp"
#include "XStack.cpp"
using namespace std;

ostream& operator<< (ostream& os, const BookItem& s)
{
	os << s.serialNum << " | "
	   << s.title << " | "
	   << s.author << " | "
	   << s.yearPublished << " | "
	   << s.copiesSold << "\n";

	return os;
}

bool operator== (const BookItem& left, const BookItem& right)
{
	if (left.serialNum == right.serialNum)
	{
		return true;
	}
	// else if (right.title.size() > 0)
	// {
	// 	string left_title = left.title;
	// 	string right_title = right.title;

	// 	transform(left_title.begin(), left_title.end(), left_title.begin(), ::tolower);
	// 	transform(right_title.begin(), right_title.end(), right_title.begin(), ::tolower);

	// 	if (left_title.find(right_title) != string::npos)
	// 		return true;
	// }

	return false;
}

string BookItem::getTitle()
{
	return this->title;
}

void BookItem::setTitle(string t)
{
	this->title = t;
}

int BookItem::getCopiesSold()
{
	return this->copiesSold;
}

void BookItem::setCopiesSold(int n)
{
	if (n >= 0) this->copiesSold = n;
}

int BookItem::getYearPublished()
{
	return this->yearPublished;
}

void BookItem::setYearPublished(int y)
{
	if (y >= 0) this->yearPublished = y;
}

bool BookItem::search(BookItem &book, XStack<BookItem>*& st, XStack<BookItem>*& search_results)
{
	Node<BookItem> * book_ptr = st->getStart();

	string left_title;
	string right_title;

	while (book_ptr != NULL)
	{
		//Match ID
		// cout << book_ptr->info << endl;
        if (book_ptr->info == book)
        {
        	search_results->push(book_ptr->info);
        	book_ptr = book_ptr->next;
        	continue;
        }

        if (book.title.length() > 0)
        {
        	// Match strings
        	left_title = book_ptr->info.title;
        	right_title = book.title;

        	// cout << book.title << " :: " << book_ptr->info.title << endl;

        	transform(left_title.begin(), left_title.end(), left_title.begin(), ::tolower);
        	transform(right_title.begin(), right_title.end(), right_title.begin(), ::tolower);

        	if (left_title.find(right_title) != string::npos)
        	{
        		search_results->push(book_ptr->info);
        	}
        }

		book_ptr = book_ptr->next;
	}

	return !search_results->isEmpty();
}

XStack<BookItem> BookItem::top5(XStack<BookItem>*& st)
{
	int counter = 0;
	XStack<BookItem> temp_st(st);
	XStack<BookItem> leaderboard;

	while (counter++ < 5)
	{
		// Find the top rated book, add to a black stack,
		//   pop it off.

		if (temp_st.count() < 1)
			break;

		BookItem topseller;
		topseller.setCopiesSold(0);

		Node<BookItem> * ptr = temp_st.getStart();

		while (ptr != NULL)
		{
			if (ptr->info.getCopiesSold() > topseller.getCopiesSold())
			{
				topseller = ptr->info; // DETHRONE!
			}
			ptr = ptr->next;
		}

		leaderboard.push(topseller);
		temp_st.remove(topseller);
	}

	leaderboard.flip();

	return leaderboard;
}

XStack<BookItem> BookItem::bottom5(XStack<BookItem>*& st)
{
	int counter = 0;
	XStack<BookItem> temp_st(st);
	XStack<BookItem> leaderboard;

	while (counter++ < 5)
	{
		// Find the top rated book, add to a black stack,
		//   pop it off.

		if (temp_st.count() < 1)
			break;

		BookItem worstseller;
		worstseller.setCopiesSold(1000);

		Node<BookItem> * ptr = temp_st.getStart();

		while (ptr != NULL)
		{
			if (ptr->info.getCopiesSold() < worstseller.getCopiesSold())
			{
				worstseller = ptr->info; // DETHRONE!
			}
			ptr = ptr->next;
		}

		leaderboard.push(worstseller);
		temp_st.remove(worstseller);
	}

	leaderboard.flip();

	return leaderboard;
}

XStack<BookItem> BookItem::newest10(XStack<BookItem>*& st)
{
	int counter = 0;
	XStack<BookItem> temp_st(st);
	XStack<BookItem> leaderboard;

	while (counter++ < 10)
	{
		// Find the top rated book, add to a black stack,
		//   pop it off.

		if (temp_st.count() < 1)
			break;

		BookItem newcomer;
		newcomer.setYearPublished(0);

		Node<BookItem> * ptr = temp_st.getStart();

		while (ptr != NULL)
		{
			if (ptr->info.getYearPublished() > newcomer.getYearPublished())
			{
				newcomer = ptr->info; // DETHRONE!
			}
			ptr = ptr->next;
		}

		leaderboard.push(newcomer);
		temp_st.remove(newcomer);
	}

	leaderboard.flip();

	return leaderboard;
}