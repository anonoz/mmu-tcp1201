#include <iostream>

using namespace std;

class AA
{
  static int x;

public:
  AA(){ x++; }
  ~AA(){ x--; }

  static int getX()
  {
    return x;
  }
};

int AA::x = 0;

int main()
{
  cout << AA::getX() << endl;
  AA* a = new AA[100];
  cout << AA::getX() << endl;
  delete [] a;
  cout << AA::getX() << endl;
}