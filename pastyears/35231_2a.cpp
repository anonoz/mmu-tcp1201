#include <iostream>

using namespace std;

template<typename X, typename Y>
class Container
{
  X key;
  Y value;
public:
  Container(X key, Y value): key(key), value(value) {};
  X getKey() const { return key; }
  Y getValue() const { return value; }
};

int main()
{
  Container<string, int> c1("Ten", 10);
  cout << c1.getKey() << " " << c1.getValue() << endl;
  Container<int, string> c2(5, "Five");
  cout << c2.getKey() << " " << c2.getValue() << endl;
}