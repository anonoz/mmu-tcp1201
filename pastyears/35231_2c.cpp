#include <iostream>

using namespace std;

class Computer
{
public:
  virtual void display() = 0;
};

class Laptop: public Computer {
public:
  void display(){ cout << "I am portable" << endl; }
};

class Desktop: public Computer {
public:
  void display(){ cout << "I am NOT portable" << endl; }
};

int main()
{
  Computer * p = new Laptop;
  p->display();
  delete p;
  p = new Desktop;
  p->display();
  delete p;
}