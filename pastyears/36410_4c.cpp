#include <iostream>

using namespace std;

class Distance
{
  int feet;
  float inches;
public:
  Distance(int ft = 0, float inch = 0)
  { feet = ft; inches = inch; }

  void display()
  {
    cout << "Feet : " << feet << "\tInches :" << inches << endl;
  }
  friend Distance operator+ (Distance ob1, Distance ob2);
  friend Distance operator- (Distance ob1, Distance ob2);
};

Distance operator+ (Distance ob1, Distance ob2)
{
  // Could've worked, but since inches is in float
  // Distance obx(static_cast<int>((ob1.inches + ob2.inches) / 12) + ob1.feet + ob2.feet, (ob1.inches + ob2.inches) % 12);

  int sum_feet = ob1.feet + ob2.feet;
  float sum_inches = ob1.inches + ob2.inches;

  if (sum_inches > 12.0) {sum_feet++; sum_inches -= 12.0;}

  return Distance(sum_feet, sum_inches);
}

Distance operator- (Distance ob1, Distance ob2)
{
  // Could've worked, but since inches is in float
  // Distance obx(static_cast<int>((ob1.inches + ob2.inches) / 12) + ob1.feet + ob2.feet, (ob1.inches + ob2.inches) % 12);

  int sum_feet = ob1.feet - ob2.feet;
  float sum_inches = ob1.inches - ob2.inches;

  if (sum_inches < 0.0) {sum_feet--; sum_inches += 12.0;}

  return Distance(sum_feet, sum_inches);
}

int main()
{
  Distance ob1(30, 8), ob2(15, 5), ob3, ob4;
  ob3 = ob1 + ob2;
  ob4 = ob1 - ob2;

  ob3.display();
  ob4.display();
}