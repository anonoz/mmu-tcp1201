#include <iostream>

using namespace std;

class Animal
{
public:
  void who() { cout << "I am animal." << endl; }
  void move() { cout << "I move." << endl; }
};

// Qi:
class Mammal: public Animal
// Qii:
// class Mammal: virtual public Animal
{
public:
  void who() { cout << "I am mammal." << endl; }
  void feed() { cout << "Drink my milk." << endl; }
};

// Qi:
class Aquatic: public Animal
// Qii:
// class Aquatic: virtual public Animal
{
public:
  void swim() { cout << "That's how I move." << endl; }
};

class Whale: public Mammal, public Aquatic
{
};

int main()
{
  Whale sammy;
  sammy.who();
  sammy.move();
}

/**
 Compilcation error message when using bad example for Question i:
 ================================================================
 
 35231_1b.cpp:38:9: error: member 'who' found in multiple base classes of
       different types
   sammy.who();
         ^
 35231_1b.cpp:18:8: note: member found by ambiguous name lookup
   void who() { cout << "I am mammal." << endl; }
        ^
 35231_1b.cpp:8:8: note: member found by ambiguous name lookup
   void who() { cout << "I am animal." << endl; }
        ^
 35231_1b.cpp:39:9: error: non-static member 'move' found in multiple base-class
       subobjects of type 'Animal':
     class Whale -> class Mammal -> class Animal
     class Whale -> class Aquatic -> class Animal
   sammy.move();
         ^
 35231_1b.cpp:9:8: note: member found by ambiguous name lookup
   void move() { cout << "I move." << endl; }
        ^
 2 errors generated.
*/